export function areParenthesesBalanced(inputString) {
  // TODO: write your code here
  let stack = [];

  let open = {
    '(': ')'
  };

  let closed = {
    ')': true
  }

  for (let i = 0; i < inputString.length; i++) {

    let char = inputString[i];

    if (open[char]) {
      stack.push(char);
    } else if (closed[char]) {
      if (open[stack.pop()] !== char) return false;
    }
  }
  return stack.length === 0;
}
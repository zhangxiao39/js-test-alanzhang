export function getFizzBuzzUntil(n) {
  // TODO: write your code here
  return Array.apply(null, { length: n }).map(function (val, index) {
    index++;
    if (index % 15 == 0) { return "FizzBuzz"; }
    if (index % 3 == 0) { return "Fizz"; }
    if (index % 5 == 0) { return "Buzz"; }
    return index;
  });
}

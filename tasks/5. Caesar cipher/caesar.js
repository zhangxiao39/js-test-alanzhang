export function encryptCaesar(inputString, key) {
  // TODO: write your code here
  const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  // Encoded Text
  let encodedText = '';

  // Iterate Over Data
  let i = 0;
  while (i < inputString.length) {
    // Valid Alphabet Characters
    const alphabetIndex = alphabet.indexOf((inputString[i]).toUpperCase());

    // Alphabet Index Is In Alphabet Range
    if (alphabet[alphabetIndex + key]) {
      // Append To String
      encodedText += alphabet[alphabetIndex + key];
    }
    // Alphabet Index Out Of Range (Adjust Alphabet By 26 Characters)
    else {
      // Append To String
      encodedText += alphabet[alphabetIndex + key - 26];
    }

    // Increase I
    i++;
  }

  return encodedText;
}

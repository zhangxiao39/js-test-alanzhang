export function Reduce(array, reducer, initialValue) {
  // TODO: write your code here
  var str = initialValue;
  for (let i = 0; i < array.length; i++) {
    str = reducer(str, array[i]);
  }
  return str;
}